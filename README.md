PRTG Device Template for HikVision Camera
==============================================
![HikVision Status](https://us.hikvision.com/sites/all/themes/hikvision_new/logo.png)


This project contains all the files necessary to integrate the HikVision Camera
into PRTG for auto discovery and sensor creation.

Sensors
====
HikVision Status

![HikVision Status](./Images/HikVision_Status.png)



HikVision Device

![HikVision Device](./Images/HikVision_Device.png)


HikVision Volume

![HikVision Volume](./Images/HikVision_Volume.png)


Download Instructions
=========================
[A zip file containing all the files in the project can be downloaded from the 
repository](https://gitlab.com/PRTG/Device-Templates/hikvision/-/jobs/artifacts/master/download?job=PRTGDistZip) 


Installation Instructions
=========================
Please see: [Generic PRTG custom sensor instructions INSTALL_PRTG.md](./INSTALL_PRTG.md)
